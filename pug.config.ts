import * as date from 'date-fns'
import * as dateAllLocales from 'date-fns/locale'

import { getL10n, isL10nExistFor } from './src/l10n/getL10n'

import MarkdownIt from 'markdown-it'
import { get } from 'lodash'
import { getResumePathFromArgs } from './src/getResumePathFromArgs'
import { markdownItNonBreakingSpaceBeforePunctuation } from './src/markdownItNonBreakingSpaceBeforePunctuation'
import { readFileSync } from 'fs'
import slugify from 'slugify'

const dateLocales = new Map([
	['en', dateAllLocales.enUS],
	['fr', dateAllLocales.fr],
])

const resumePath = getResumePathFromArgs(process.argv)
const resume = JSON.parse(readFileSync(resumePath, 'utf8'))

const resumeLocale = get(
	resume,
	'basics.location.countryCode',
	'',
).toLowerCase()
const locale =
	isL10nExistFor(resumeLocale) && dateLocales.has(resumeLocale)
		? resumeLocale
		: 'en'

const themeLocale = getL10n(locale)
const dateLocale = dateLocales.get(locale)

const dateOptionWithLocale = {
	locale: dateLocale,
}

const markdownIt = new MarkdownIt({
	html: true,
	xhtmlOut: true,
	typographer: true,
	quotes: '“”‘’',
})

markdownItNonBreakingSpaceBeforePunctuation(markdownIt)

const markdown = {
	block(input: string): string {
		return markdownIt.render(input)
	},

	inline(input: string): string {
		return markdownIt.renderInline(input)
	},
}

function br2nl(html: string) {
	return html.replace(/(<br( [^>]*)?>)/gm, ' ')
}

function removeHtml(html: string) {
	return html.replace(/(<([^>]+)>)/gm, '')
}

type UnknownDateType = string | number | Date

function dateParse(maybeDate: UnknownDateType): Date {
	if (typeof maybeDate === 'string') {
		return date.parseISO(maybeDate)
	}

	if (typeof maybeDate === 'number') {
		return new Date(maybeDate)
	}

	return maybeDate
}

interface ItemWithStartDate {
	startDate: string
}

function compareByStartDate(
	{ startDate: a }: ItemWithStartDate,
	{ startDate: b }: ItemWithStartDate,
): number {
	return date.compareDesc(dateParse(a), dateParse(b))
}

module.exports = {
	locals: {
		locale,
		themeLocale,
		date: {
			differenceInCalendarYears: (
				dateLeft: UnknownDateType,
				dateRight: UnknownDateType,
			): number =>
				date.differenceInCalendarYears(
					dateParse(dateLeft),
					dateParse(dateRight),
				),
			differenceInCalendarMonths: (
				dateLeft: UnknownDateType,
				dateRight: UnknownDateType,
			): number =>
				date.differenceInCalendarMonths(
					dateParse(dateLeft),
					dateParse(dateRight),
				),
			isEqual: date.isEqual,
			formatDistanceToNow: (maybeDate: UnknownDateType) =>
				date.formatDistanceToNow(dateParse(maybeDate), dateOptionWithLocale),
			formatDistance: (maybeDate: UnknownDateType, baseDate: UnknownDateType) =>
				date.formatDistance(
					dateParse(maybeDate),
					dateParse(baseDate),
					dateOptionWithLocale,
				),
			format: (maybeDate: UnknownDateType, format: string) =>
				date.format(dateParse(maybeDate), format, dateOptionWithLocale),
		},
		urlify(...input: string[]): string {
			let slug = input.filter(_ => _).join(' ')
			slug = br2nl(slug)
			slug = removeHtml(slug)
			slug = slugify(slug, { lower: true })
			return slug
		},
		resume,
		markdown,
		compareByStartDate,
	},
}
