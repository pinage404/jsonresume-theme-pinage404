module.exports = {
	plugins: [
		require('cq-prolyfill/postcss-plugin')(),
		require('postcss-nested')(),
	],
}
