import Bundler, { ParcelOptions } from 'parcel-bundler'
import { resolve } from 'path'

import { promises as fs } from 'fs'

const rootFilePath = resolve(__dirname, 'index.pug')

const resumePath = resolve(__dirname, '..', 'resume.json')

const parcelOption: ParcelOptions = {
	publicUrl: '.',
	watch: true,
}
if (process.argv.includes('export')) {
	parcelOption.watch = false
}

const bundler = new Bundler(rootFilePath, parcelOption)

async function build() {
	const bundleFilePath = (await bundler.bundle()).name

	const fileContent = await fs.readFile(bundleFilePath, 'utf8')

	return fileContent
}

export const render = async (resume: object) => {
	try {
		const resumeJson = JSON.stringify(resume, null, '\t')
		await fs.writeFile(resumePath, resumeJson)

		return await build()
	} catch (ex) {
		console.error(ex)
		return 'Server error'
	}
}

async function main(argv: string[]) {
	if (argv.includes('export')) {
		const outputHTML = await build()

		const outputPath = argv[argv.indexOf('export') + 1]
		await fs.writeFile(outputPath, outputHTML)
	}
}

main(process.argv)
