import MarkdownIt from 'markdown-it'
import StateCore from 'markdown-it/lib/rules_core/state_core'

const normalSpaceFollowedByPunctuationPattern = / ([:;!?])([ \n]|$)/gm
const nonBreakingSpaceFollowedByPunctuation = ' $1$2'

const rule = (state: StateCore): boolean => {
	state.tokens
		.filter(token => token.children !== null)
		.forEach(token => {
			token.children!.forEach(token => {
				token.content = token.content.replace(
					normalSpaceFollowedByPunctuationPattern,
					nonBreakingSpaceFollowedByPunctuation,
				)
			})
		})
	return true
}

export function markdownItNonBreakingSpaceBeforePunctuation(
	markdownIt: MarkdownIt,
): void {
	markdownIt.core.ruler.push('non_breaking_space_before_punctuation', rule)
}
