import { resolve } from 'path'

function findOptionValue(argv: string[], option: string): string | undefined {
	if (argv.includes(option)) {
		return argv[argv.indexOf(option) + 1]
	}

	return argv
		.find(arg => arg.startsWith(`${option}=`))
		?.replace(`${option}=`, '')
}

export function getResumePathFromArgs(argv: string[]): string {
	return resolve(
		findOptionValue(argv, '--resume') ||
			findOptionValue(argv, '-r') ||
			resolve(__dirname, '..', 'resume.json'),
	)
}
