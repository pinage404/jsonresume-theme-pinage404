import { load as parseYaml } from 'js-yaml'
import { readFileSync } from 'fs'
import { resolve } from 'path'

type Locale = {
	readonly [lang: string]: string
}

type Locales = {
	readonly [lang: string]: Locale
}

function loadL10n(lang: string): Locale {
	const path = resolve(__dirname, `${lang}.yaml`)
	try {
		const content = readFileSync(path, 'utf8')
		const l10n = parseYaml(content)
		if (typeof l10n !== 'object' || l10n === null) {
			throw new Error("Locale's YAML should be a map/object")
		}
		return l10n as Locale
	} catch (ex) {
		console.error(`Can't open ${path}`, ex)
		throw ex
	}
}

function getAvailableLocales(): Locales {
	return {
		get en() {
			return loadL10n('en')
		},
		get fr() {
			return loadL10n('fr')
		},
	}
}

export function isL10nExistFor(inputLocale: string): boolean {
	return inputLocale.toLowerCase() in getAvailableLocales()
}

export function getL10n(inputLocale: string): Locale {
	const locale = isL10nExistFor(inputLocale)
		? (inputLocale as keyof Locales)
		: 'en'

	return getAvailableLocales()[locale]
}
